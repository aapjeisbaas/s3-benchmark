> This is not a fully fledged polished tool, it might be good inspiration for that ;-)

In a search for high throughput data reading from S3 I used the scripts in here to validate and automatically test my work.

The results for r5.* and r5n.* can be found in: `summary.csv`

I used goofys for these tests, I gave s3fs a try but it is just not as fast.


```
for type in $(cat summary.csv | sort -h | grep -ve type | awk '{print $1}' | uniq); do grep $type summary.csv | sort -h -k 3 | tail -n 1; done | sort -r -h -k 3
r5n.12xlarge, 9, 5442570268
r5n.24xlarge, 9, 4763634411.5
r5n.16xlarge, 10, 4581923737.5
r5n.8xlarge, 10, 4263770986
r5n.4xlarge, 10, 3021756509
r5.24xlarge, 10, 2985506326
r5.16xlarge, 7, 2528126760
r5n.2xlarge, 8, 1780095946
r5.12xlarge, 8, 1517019153.5
r5.8xlarge, 8, 1264198070.5
r5.4xlarge, 7, 1264194381.5
r5.2xlarge, 9, 1264140908
r5.xlarge, 10, 977006162
r5n.xlarge, 4, 841889850
r5.large, 3, 456765753
r5n.large, 5, 448342029.5
```

![Google Analytics](https://www.google-analytics.com/collect?v=1&tid=UA-48206675-1&cid=555&aip=1&t=event&ec=repo&ea=view&dp=gitlab%2Fs3-benchmark%2FREADME.md&dt=s3-benchmark)
