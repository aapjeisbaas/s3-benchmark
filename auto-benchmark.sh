#!/bin/bash

TypeArray=("r5n.large"  "r5n.xlarge"  "r5n.2xlarge"  "r5n.4xlarge"  "r5n.8xlarge" "r5n.12xlarge" "r5n.16xlarge" "r5n.24xlarge"
            "r5.large"  "r5.xlarge"  "r5.2xlarge"  "r5.4xlarge"  "r5.8xlarge" "r5.12xlarge" "r5.16xlarge" "r5.24xlarge")

function running {
    lines=$(aws ec2 --profile vicky describe-instances --filters Name=spot-instance-request-id,Values=$(cat instance.json | jq '.SpotInstanceRequests[0] .SpotInstanceRequestId' -r) Name=instance-state-name,Values=running --output json | wc -l)
    if [ "$lines" -gt "5" ]; then
        true
    else
        false
    fi
}

for type in ${TypeArray[*]}; do
    ./specgen.sh $type
    echo ""
    echo "Running benchmark on: $type"
    aws ec2 --profile vicky request-spot-instances --spot-price "10" --instance-count 1 --type "one-time" --launch-specification file://specification.json > instance.json
    sleep 60
    while running; do
        sleep 10
    done
done