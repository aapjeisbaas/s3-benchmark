#!/bin/bash

SPEED=$(cat $1)
TYPE=$(echo $1 | sed 's/\.\/benchmarks\///g; s/\/.*//g')
THREADS=$(echo $1 | sed 's/.*dstat_//g; s/\..*//g')

echo "$TYPE, $THREADS, $SPEED" >> summary.csv

