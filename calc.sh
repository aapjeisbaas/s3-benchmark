aws s3 --profile vicky sync s3://aapjeisbaas-benchmark/benchmarks/ benchmarks/
cd benchmarks
find . -type f -size -2k -delete
find . -type f | xargs -n 1 -P 16 -I% sed -i -n '/recv/,$p' %
find . -type f | xargs -n 1 -P 16 -I% R -q -e "x <- read.csv(file='%',head=TRUE,sep=","); fileConn<-file('%.median'); writeLines(as.character(median(x\$recv)), fileConn); close(fileConn)"
cd -
rm summary.csv
echo "type, threads, speed" >> summary.csv
find . -type f -iname "*median" | xargs -n 1 -P 16 -I% ./summary.sh %
