#!/bin/bash
useradd --groups wheel svanbroekhoven
mkdir /home/svanbroekhoven/.ssh/
echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDB2YKsCeu6S9WBbBoaM0v0r9F5Zy0CnZO4bSDyiJDkxfEC8s4etXR1Yafg5ptB/b9AupemzFSEUfIOz46Gy/T2MiMGVufzmwC/BfMEXvNLhbagVrHESKOmJV3NAnbN6qBXXVtjOQFiMvwmtRrRi4MDLnessTeW2tqhfimth0a3n5wgyZpBr2I9yz7RL+6guqRi9hnjj75+XdAdDF7jXvfKEcb/qeFyeCtgpCdR4LHPm9Dah00eNnWrnQQg/kmYAkLpbTEszHMyl2S24W14eEfd6Kbq2yMRZExRyEaGRkSuEFwW8lvV5pOBksGYMaYIe17LiiterH6owJ6ukyj7jbUv svanbroekhoven@vpn.aapjeisbaas.nl' >> /home/svanbroekhoven/.ssh/authorized_keys
chown svanbroekhoven:svanbroekhoven /home/svanbroekhoven/.ssh -R
chmod 600 /home/svanbroekhoven/.ssh/authorized_keys
echo '%wheel         ALL = (ALL) NOPASSWD: ALL' >> /etc/sudoers


amazon-linux-extras install epel -y
yum install -y s3fs-fuse

mkdir /media/s3
s3fs -o iam_role="S3-benchmarks" aapjeisbaas-benchmark /media/s3
s3fs -o use_cache="" -o iam_role="S3-benchmarks" -o parallel_count="30" aapjeisbaas-benchmark /media/s3

yum install -y dstat

TYPE=$(curl -s http://169.254.169.254/latest/meta-data/instance-type)
mkdir -p "/media/s3/benchmarks/$TYPE"

sleep 2

for i in {1..10}; do
    echo copy with $i threads
    rm "/media/s3/benchmarks/$TYPE/dstat_$i.csv"
    dstat --output "/media/s3/benchmarks/$TYPE/dstat_$i.csv"&
    find /media/s3/data/ -type f | shuf | head -n $i | xargs -n 1 -P $i -I% cat '%' >> /dev/null
    pkill dstat
    sleep 2
    umount -l /media/s3
    sleep 2
    pkill s3fs
    s3fs -o iam_role="S3-benchmarks" aapjeisbaas-benchmark /media/s3
    sleep 2
done

ID=$(curl -s http://169.254.169.254/latest/meta-data/instance-id)
aws --region eu-west-1 ec2 terminate-instances --instance-ids "$ID"
