#!/bin/bash

type=$1
userdata=$(cat user-data.sh| base64 -w 0)
echo '''
{
    "InstanceType": "'${type}'",
    "ImageId": "ami-06ce3edf0cff21f07",
    "SecurityGroupIds": [ "sg-05659014917d55c56" ],
    "IamInstanceProfile": {
        "Arn": "arn:aws:iam::616258975614:instance-profile/S3-benchmarks"
    },
    "SubnetId": "subnet-0379c9d3b3fa3c74e",
    "Placement": {
        "AvailabilityZone": "eu-west-1c"
    },
    "UserData": "'${userdata}'"
}
''' > specification.json